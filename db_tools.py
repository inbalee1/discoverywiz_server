# coding=utf-8

from datetime import timedelta
import datetime
from entities.entity import Session, engine, Base
from entities.member import Member, MemberSchema
import datetime

# base function for first creation of the postgres db
def init_postgres():
	Base.metadata.drop_all(engine)
	Base.metadata.create_all(engine)
	print('init_postgres')


# adding new member
def add_this_user(email, phonenum, password, gender, member_type):
    session = Session()
    new_user = Member(email, phonenum, password, gender, member_type)
    session.add(new_user)
    session.commit()
    print(new_user)
    return new_user

# get member by phone number
def get_member_by_phone(phonenum):
    query_data = {}
    try:
        session = Session()
        member = session.query(Member).filter(Member.phonenum == phonenum).one()
        query_data['email'] = member.email
        session.close()
    except:
        pass
    return query_data