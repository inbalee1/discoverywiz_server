from datetime import datetime
from sqlalchemy import create_engine, Column, String, Integer, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

db_url = 'localhost:5432'
db_name = 'discoverywiz'
db_user = 'inbal'
db_password = 'startup123'
engine = create_engine('postgresql://'+db_user+':'+db_password+'@'+db_url+'/'+db_name)


# engine = create_engine('postgresql://inbal:startup123@localhost:5432/discoverywiz')
Session = sessionmaker(bind=engine)

Base = declarative_base()

class Entity():
    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime)

    def __init__(self):
        self.created_at = datetime.now()
