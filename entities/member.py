# coding=utf-8
from datetime import datetime
from sqlalchemy import Column, String
from .entity import Entity, Base
from marshmallow import Schema, fields
from sqlalchemy.schema import UniqueConstraint
import sqlalchemy as sa

class MemberSchema(Schema):
    id = fields.Number()
    email = fields.Str()
    phonenum = fields.Str()    
    password = fields.Str()        
    gender = fields.Str()
    member_type = fields.Str()    
    created_at = fields.DateTime()

class Member(Entity, Base):
    __tablename__ = 'members'
    __table_args__ = (sa.UniqueConstraint('phonenum'),)

    email = Column(String)
    phonenum = Column(String)
    password = Column(String)
    gender = Column(String)
    member_type = Column(String)

    def __init__(self, email, phonenum, password, gender, member_type):
        Entity.__init__(self)
        self.email = email
        self.phonenum = phonenum
        self.password = password
        self.gender = gender
        self.member_type = member_type
        self.created_at = datetime.now()
        
