from starlette.applications import Starlette
from starlette.responses import JSONResponse, HTMLResponse, RedirectResponse,Response
from starlette.requests import ClientDisconnect, Request, State
from starlette.routing import Route
import sys
from io import BytesIO
import uvicorn
import aiohttp
from starlette.middleware.cors import CORSMiddleware
from starlette.middleware import Middleware
from db_tools import init_postgres,add_this_user,get_member_by_phone
from utils import star_email
    	
async def get_bytes(url):
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            return await response.read()


async def register(request):
    data = await request.json() #[{'name': 'account', 'value': 'founder'}, {'name': 'phone', 'value': '0546640680'}, {'name': 'name', 'value': 'asd@asd.com'}, {'name': 'name', 'value': '123'}, {'name': 'gender', 'value': 'w'}]
    dict_data={}
    for item in data:
    	dict_data[item["name"]]=item["value"]
    try:
    	res=add_this_user(dict_data['email'], dict_data['phone'],dict_data['password'], dict_data['gender'], dict_data['account'])
    	return JSONResponse({"message":"Regsitrated successfully!"})
    except Exception as e:
    	c = type(e).__name__
    	if c =='IntegrityError':
    		member=get_member_by_phone(dict_data['phone'])
    		print(member['email'])
    		new_email=star_email(member['email'])
    		print('Error! Code: {c}'.format(c = type(e).__name__))
    		msg = "Your phone number already exists in the system. Please check your email "+star_email(member['email'])
    		return JSONResponse({"message":msg})
    	else:
    		return JSONResponse({"message":"Unkown error please contact us!"})	    	
	
    


routes = [
    Route("/register", endpoint=register,methods=["GET","POST"]),
]



app = Starlette(routes=routes)


origins = [
    "http://localhost",
    "http://localhost:3001",    
    "http://localhost:8080",
    "http://localhost:8888",
    "http://44.231.99.120", # replace with private ip
    "http://172.31.21.169" # replace with public ip
    
]


app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.route("/init_postgres_request", methods=["GET"])
def init_postgres_request():
	init_postgres()

