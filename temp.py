from flask import Flask, render_template, request,jsonify
from flask_cors import CORS, cross_origin
from db_tools import init_postgres,add_this_user,get_member_by_phone
from utils import star_email

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

@app.route("/init_postgres_request", methods=["GET", "POST"])
@cross_origin()
def init_postgres_request():
	init_postgres()
	print('echo called')
	return "hello"
   
@app.route("/register", methods=["GET", "POST"])
@cross_origin()    
def register():
	try:
		dict_data=request.form
		res=add_this_user(dict_data['email'], dict_data['phone'],dict_data['password'], dict_data['gender'], dict_data['account'])
		return jsonify({"message":"Regsitrated successfully!"})
	except Exception as e:
		c = type(e).__name__
		if c =='IntegrityError':
			member=get_member_by_phone(dict_data['phone'])
			new_email=star_email(member['email'])
			print('Error! Code: {c}'.format(c = type(e).__name__))
			msg = "Your phone number already exists in the system. Please check your email "+star_email(member['email'])
			return jsonify({"message":msg})
		else:
			return jsonify({"message":"Unkown error please contact us!"})	 
   

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8787, debug=True)
