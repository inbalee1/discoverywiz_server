# coding=utf-8
import re

def star_email(s):
    firstchar = s[0]
    email_arr = s.split("@")
    len_email_to_star=len(email_arr[0])-1
    started_str = "*" * len_email_to_star
    new_str = firstchar + started_str + "@" + email_arr[1]
    return new_str
