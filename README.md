sudo apt-get install language-pack-UTF-8

#install python3.7
* sudo nano /etc/apt/sources.list
* paste: deb http://ftp.de.debian.org/debian stable main
* sudo apt-get update
* sudo apt-get install libpq-dev
* sudo apt-get -t stable install python3.7
* make sure python3.7 is installed 
* python3.7 -V -> should show: Python 3.7.3
* sudo apt-get install python3-venv    
* --not sure it's mandatory but I did it--
* sudo nano /etc/apt/sources.list
* remove: deb http://ftp.de.debian.org/debian stable main


#install postgres
* echo 'deb http://apt.postgresql.org/pub/repos/apt/ jessie-pgdg main' | sudo tee /etc/apt/sources.list.d/pgdg.list
* wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
* sudo apt-get update
* sudo apt-get install postgresql-9.6 -y    
* check postgres installations: 
* psql -V -> show show: psql (PostgreSQL) 9.6.16

#install more libs

* sudo apt-get install python-psycopg2 

#upload the folder (get from git, I've updated some stuff like requirements)
* cd discoverywiz_server
* python3.7 -m venv new_venv
* source new_venv/bin/activate
* pip install -r requirements.txt
* python temp.py -> to test!





